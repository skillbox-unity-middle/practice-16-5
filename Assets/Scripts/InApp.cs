using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InApp : MonoBehaviour
{
    [SerializeField] private CharacterData _character;

    public void OnPurchaseCompleted(int reward)
    {
        _character.AddScore(reward);
    }
}
